package main

type Thumbnail struct {
	URL string `json:"url"`
}

type Footer struct {
	Text    string `json:"text"`
	IconUrl string `json:"icon_url"`
}

type Field struct {
	Name   string `json:"name"`
	Value  string `json:"value"`
	Inline bool   `json:"inline"`
}

type Embed struct {
	Inline      bool      `json:"inline"`
	Title       string    `json:"title"`
	Username    string    `json:"username"`
	Color       int       `json:"color"`
	Thumbnail   Thumbnail `json:"thumbnail"`
	Footer      Footer    `json:"footer"`
	Description string    `json:"description"`
	Fields      []Field   `json:"fields"`
}

type Message struct {
	Embeds []Embed `json:"embeds"`
}