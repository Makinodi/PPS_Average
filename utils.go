package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
)

/*
iface:
        eth0, ...
channel:
        rx_bytes | rx_packets
        tx_bytes | tx_packets
*/
func GetStat(iface, channel string) (int, error) {
	file, err := os.Open(fmt.Sprintf("/sys/class/net/%s/statistics/%s", iface, channel))
	if err != nil {
		return -1, err
	}

	buffer, err := ioutil.ReadAll(file)
	if err != nil {
		return -1, err
	}

	value, err := strconv.Atoi(strings.TrimSpace(string(buffer)))
	if err != nil {
		return -1, err
	}

	return value, nil
}

func average(slice []int) int {
	total := 0

	for _, n := range slice {
		total += n
	}
	return total / len(slice)
}

func PostWebhook(webhookURL string, averagePPS int) { //, averageMBytes int) {
	buffer, _ := json.Marshal(Message{Embeds: []Embed{
		{
			Inline:   false,
			Title:    "We no longer detected an attack on IP address 142.252.25****",
			Username: "Attack Detection",
			Color:    3066993,
			Thumbnail: Thumbnail{
				URL: "https://cdn.discordapp.com/attachments/783466029551452170/787855977864101928/blockdos-ddos-protection.png",
			},
			Footer: Footer{
				Text:    "SharooSecurityVPN",
				IconUrl: "https://cdn.discordapp.com/attachments/783466029551452170/787855977864101928/blockdos-ddos-protection.png",
			},
			Description: "The server has now been withdrawn from our mitigation system.",
			Fields: []Field{
				{
					Name:   "Average PPS",
					Value:  fmt.Sprintf("%dPPS", averagePPS),
					Inline: false,
				},
				/*{
					Name:   "Average Speed",
					Value:  fmt.Sprintf("%dMb/s", averageMBytes),
					Inline: false,
				},*/
			},
		},
	}})
	resp, err := http.Post(webhookURL, "application/json", bytes.NewBuffer(buffer))
	if err != nil {
		log.Println(err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		fmt.Printf("Unexpected response code: %d", resp.StatusCode)
	}
}
