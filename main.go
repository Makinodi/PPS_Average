package main

import (
	"flag"
	"fmt"
	"log"
	"time"
)

var (
	webhookURL string
	trigger    int
)

func init() {
	flag.StringVar(&webhookURL, "webhook", "", "Discord webhook URL")
	flag.IntVar(&trigger, "trigger", 0, "Trigger value")
	flag.Parse()
}

// TODO: Rework Bytes per sec calculation

func main() {
	state := false
	var buffPPS []int
	//var buffBytes []int

	for {
		currentPPS, err := GetStat("eth0", "rx_packets")
		if err != nil {
			log.Fatal(err)
		}

		/*currentBytes, err := GetStat("eth0", "rx_bytes")
		if err != nil {
			log.Fatal(err)
		}*/

		time.Sleep(2 * time.Second)

		lastPPS, err := GetStat("eth0", "rx_packets")
		if err != nil {
			log.Fatal(err)
		}
		/*lastBytes, err := GetStat("eth0", "rx_bytes")
		if err != nil {
			log.Fatal(err)
		}*/
		diffPPS := lastPPS - currentPPS
		//diffBytes := lastBytes - currentBytes

		if !state && diffPPS > trigger {
			buffPPS = append(buffPPS, diffPPS)
			//buffBytes = append(buffBytes, diffBytes)
			state = true
		}
		if state && diffPPS > trigger {
			buffPPS = append(buffPPS, diffPPS)
			//buffBytes = append(buffBytes, diffBytes)
		}
		if state && diffPPS < trigger {
			state = false
			fmt.Println("something was fishy")
			fmt.Println(fmt.Sprintf("Average PPS: %dpps", average(buffPPS)))
			//fmt.Println(fmt.Sprintf("Average speed: %dMb/s", (average(buffBytes)/1024)/1024))
			PostWebhook(webhookURL, average(buffPPS)) //, (average(buffBytes)/1024)/1024)
			buffPPS = []int{}
			//buffBytes = []int{}
		}
	}
}
